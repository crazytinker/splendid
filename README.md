# splendid
Wordpress Theme

## credits
- Design inspired by Fateem Diabate
- [Images](https://www.negativespace.co/?ref=pexels)

### tools
- http://www.scriptalicious.com/tools/strip-html-tags/
- https://codex.wordpress.org/Child_Themes
- https://codex.wordpress.org/Theme_Development
